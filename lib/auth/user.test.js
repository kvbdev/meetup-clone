import { extractUserProfileObject } from './user'

it('converts auth0 profiles into our user profiles', () => {
  const exampleProfile = {
    given_name: 'Richard',
    family_name: 'Vanbergen',
    nickname: 'rich.vanbergen',
    name: 'Richard Vanbergen',
    picture: 'https://lh3.googleusercontent.com/a-/AAuE7mBU7BM4qIn1CgTjNI_QeV3bvbNPZQoez7aI8fuW',
    locale: 'en',
    updated_at: '2020-01-17T02:29:35.617Z',
    sub: 'google-oauth2|112750929694122164612'
  }

  expect(extractUserProfileObject(exampleProfile)).toMatchObject({
    username: 'rich.vanbergen',
    name: 'Richard Vanbergen',
    picture: 'https://lh3.googleusercontent.com/a-/AAuE7mBU7BM4qIn1CgTjNI_QeV3bvbNPZQoez7aI8fuW',
    locale: 'en'
  })
})
