import { initAuth0 } from '@auth0/nextjs-auth0'
import { extractUserProfileObject } from './user'

const auth0 = () => {
  const [protocol, host, domain, clientId, clientSecret] = [
    process.env.APP_PROTOCOL,
    process.env.APP_DOMAIN,
    process.env.AUTH0_DOMAIN,
    process.env.AUTH0_CLIENT_ID,
    process.env.AUTH0_CLIENT_SECRET,
  ]

  return initAuth0({
    domain,
    clientId,
    clientSecret,
    scope: 'openid profile',
    redirectUri: `${protocol}://${host}/api/auth/callback`,
    postLogoutRedirectUri: `${protocol}://${host}`,
    session: {
      // The secret used to encrypt the cookie.
      cookieSecret: process.env.AUTH0_RANDOM_COOKIE,
      // The cookie lifetime (expiration) in seconds. Set to 8 hours by default.
      cookieLifetime: 60 * 60 * 8,
    },
  })
}

export const currentUser = async (request) => {
  const auth0Instance = auth0()
  const userResponse = await auth0Instance.getSession(request)
  if (userResponse) {
    const { user } = await auth0Instance.getSession(request)
    return extractUserProfileObject(user)
  }

  return null
}

export default auth0
