export default (req, res) => {
  const {
    query: { name }
  } = req

  const greeting = `Hello ${name}`

  res.json({ greeting })
}
