const NextI18Next = require('next-i18next').default
const path = require('path')

const NextI18NextInstance = new NextI18Next({
  defaultLanguage: 'en',
  otherLanguages: ['de', 'jp'],
  localeSubpaths: {
    de: 'de',
    jp: 'jp',
  },
  localePath: path.resolve('./public/static/locales'),
})

module.exports = NextI18NextInstance
