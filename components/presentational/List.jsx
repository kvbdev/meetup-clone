import React from 'react'
import PropTypes from 'prop-types'

/**
 * Returns one string when supplied with 2 strings
 * **e.g. testStrings('test1', 'test2')**
 * @param {string} string1 First string
 * @param {string} string2 Second string
 * @returns {string} Both string with a comma
 */
const testStrings = (string1, string2) => {
  const returnString = `${string1}, ${string2}`
  return returnString
}

/**
 * Test component for checking sytax
 * Renders props and has other test functions
 * @param {object} props React component props
 * @param {string} props.testItem String to render
 */
const List = (props) => {
  const { testItem } = props
  return (
    <div>
      <ul>
        <li data-testid='first-item'>Test</li>
        <li>Test again</li>
        <li>{testItem}</li>
        <li>{testStrings('test', 'test twice')}</li>
      </ul>
    </div>
  )
}

List.propTypes = {
  testItem: PropTypes.string.isRequired
}

export default List
