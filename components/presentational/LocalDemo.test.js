import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { LocalDemo } from './LocalDemo'

jest.mock('react-i18next', () => ({
  // this mock makes sure any components using the translate HoC receive the t function as a prop
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
}))

afterEach(cleanup)

it('matches snapshot', () => {
  const { asFragment } = render(<LocalDemo t={key => key} />)
  expect(asFragment()).toMatchSnapshot()
})

it('renders with descriptionElement', () => {
  const { getByTestId } = render(<LocalDemo t={key => key} />)
  const descriptionDiv = getByTestId('descriptionElement')
  expect(descriptionDiv).toBeInTheDocument()
})

it('with description test', () => {
  const { getByTestId } = render(<LocalDemo t={key => key} />)
  const descriptionDiv = getByTestId('descriptionElement')
  expect(descriptionDiv).toHaveTextContent('description')
})
