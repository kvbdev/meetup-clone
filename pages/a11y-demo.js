import React from 'react'
import Head from 'next/head'

/**
 * Demo page showing correct composition of accessible content
 * with annotations where necessary
 */
const A11yDemo = () => {
  return (
    <>
      {/* The title should always be set as it is picked up by the screen reader and helps people understand the content */}
      <Head>
        <title>My accessible page demo</title>
      </Head>
      {/* Section headings like nav and main should always be defined as they help accessible navigation tools */}
      <main>
        <h1>Title</h1>
        <div className='main-text'>This is the main text.</div>
      </main>

      <style jsx>{`
        .main-text {
          color: red;
        }
      `}</style>
    </>
  )
}

export default A11yDemo
