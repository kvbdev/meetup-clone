import auth0 from '../../../lib/auth/auth0'

export default async function profile (req, res) {
  try {
    const { headers } = req
    const [protocol, host] = [headers['x-forwarded-proto'], headers['x-forwarded-host']]
    const auth0Instance = auth0(protocol, host)

    await auth0Instance.handleProfile(req, res)
  } catch (error) {
    console.error(error)
    res.status(error.status || 500).end(error.message)
  }
}
