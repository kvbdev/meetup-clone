export function extractUserProfileObject (auth0Profile) {
  return {
    username: auth0Profile.nickname,
    name: auth0Profile.name,
    picture: auth0Profile.picture,
    locale: auth0Profile.locale
  }
}
