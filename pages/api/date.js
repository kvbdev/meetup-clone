import { getSimpleDate } from '../../lib/dates'

export default (req, res) => {
  const date = getSimpleDate()
  res.json({ date })
}
