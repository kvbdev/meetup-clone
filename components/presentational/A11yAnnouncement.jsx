import React from 'react'

/**
 * Accessibility announcement.
 * visuallyhidden class should be defined to prevent display
 * @param {string} announcementText Text to announce
 */
const A11yAnnouncement = (announcementText = '') => {
  return (
    <div>
      <div aria-live='polite' aria-atomic='true' className='visuallyhidden'>
        {announcementText}
      </div>
    </div>
  )
}

export default A11yAnnouncement
