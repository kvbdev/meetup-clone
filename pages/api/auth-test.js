import { currentUser } from '../../lib/auth/auth0'

export default async (req, res) => {
  const user = await currentUser(req)

  if (user) {
    return res.json({ user })
  } else {
    return res.json({ message: 'no user :(' })
  }
}
