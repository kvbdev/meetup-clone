import React from 'react'
import { render } from '@testing-library/react'

import TempPage from '../pages/temp-page'

describe('With React Testing Library', () => {
  it('Shows Home link', () => {
    const { getByText } = render(<TempPage />)

    expect(getByText('Home')).not.toBeNull()
  })
})

describe('With React Testing Library Snapshot', () => {
  it('Should match Snapshot', () => {
    const { asFragment } = render(<TempPage />)

    expect(asFragment()).toMatchSnapshot()
  })
})
