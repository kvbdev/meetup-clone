import React from 'react'
import { render, cleanup } from '@testing-library/react'
import List from './List.jsx'

afterEach(cleanup)

it('matches snapshot', () => {
  const { asFragment } = render(<List testItem="test text" />)
  expect(asFragment()).toMatchSnapshot()
})

it('renders with first item', () => {
  const { getByTestId } = render(<List testItem="test text"/>)
  const listElement = getByTestId('first-item')
  expect(listElement).toBeInTheDocument()
})

it("renders with 'again' text", () => {
  const { getByText } = render(<List testItem="test text" />)
  const listElement = getByText(/again/i)
  expect(listElement).toBeInTheDocument()
})
