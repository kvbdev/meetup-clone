import React from 'react'
import PropTypes from 'prop-types'

// This is our initialised `NextI18Next` instance
import { withTranslation } from '../../i18n'

/**
 * Demo component receiving the t() function
 * For translation
 * Note t is not listed as a param as will be used everythere
 */
const LocalDemo = ({ t }) => {
  return (
    <>
      <div data-testid='descriptionElement'>
        {t('description')}
      </div>
    </>
  )
}

LocalDemo.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('localDemo')(LocalDemo)
export { LocalDemo }
