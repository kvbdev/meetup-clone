module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
    'jest/globals': true
  },
  extends: [
    'plugin:react/recommended',
    'standard',
    'plugin:jsx-a11y/recommended'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  plugins: [
    'react',
    'jest',
    'jsx-a11y'
  ],
  rules: {
    'quotes': [2, 'single', { 'avoidEscape': true }],
    'comma-dangle': ['warn', 'only-multiline'],
    'jest/no-disabled-tests': 'warn',
    'jest/no-focused-tests': 'error',
    'jest/no-identical-title': 'error',
    'jest/prefer-to-have-length': 'warn',
    'jest/valid-expect': 'error',
    'object-curly-spacing': ['error', 'always'],
    'react/destructuring-assignment': [2, 'always'],
    'react/no-unused-prop-types': [1],
    'react/prefer-stateless-function': [2],
    'react/require-default-props': [2],
    'react/jsx-key': [2],
    'react/jsx-uses-react': [2]
  },
  settings: {
    react: {
      version: 'detect',
    }
  },
  overrides: [
    {
      files: [
        "**/*.test.js"
      ],
      env: {
        jest: true
      }
    }
  ]
}
