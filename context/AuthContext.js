import React, { useReducer, createContext } from 'react'
import PropTypes from 'prop-types'

const AuthContext = createContext({
  user: null
})

const actions = {
  SET_USER: 'setUser'
}

const actionCreators = dispatch => ({
  setUser: user => {
    dispatch({
      type: actions.SET_USER,
      payload: { user }
    })
  }
})

// first paramter is your state, second is the action
const reducer = (currentState, { type, payload }) => {
  switch (type) {
    case actions.SET_USER:
      return { user: payload.user }
    default:
      return { ...currentState }
  }
}

// this component wraps any of the child components that you want to share state with
const AuthProvider = ({ children, initialState }) => {
  const [state, dispatch] = useReducer(reducer, initialState)
  const actions = actionCreators(dispatch)

  return (
    <AuthContext.Provider value={{ state, actions }}>
      {children}
    </AuthContext.Provider>
  )
}

AuthProvider.propTypes = {
  children: PropTypes.element.isRequired,
  initialState: PropTypes.object.isRequired
}

export { AuthContext, AuthProvider }
