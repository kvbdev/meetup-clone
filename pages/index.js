import React, { useState, useEffect, useContext, useCallback, useRef } from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { get } from 'axios'
import { getSimpleDate } from '../lib/dates'

import Layout from '../components/layout/Main'
import { AuthContext } from '../context/AuthContext'
import { attributes, react as HomeContent } from '../content/home.md'

const Home = ({ date }) => {
  // comes from the markdown file
  const { title, cats } = attributes

  const [greeting, setGreeting] = useState('loading')
  const [identityResponse, setIdentityResponse] = useState(null)
  const [random, setRandom] = useState(Math.random())
  const [dateProperty, setDateProperty] = useState(date)
  const [isSending, setIsSending] = useState(false)

  const isMounted = useRef(true)
  useEffect(() => {
    return () => {
      isMounted.current = false
    }
  }, [])

  const originalDate = date

  const {
    state: { user }
  } = useContext(AuthContext)

  // this greeting is a plain function with no identity and no db
  useEffect(() => {
    const fetchData = async () => {
      const helloResponse = await get(
        `/api/greeting?name=${random}`
      )

      const { data: { greeting } } = helloResponse

      setGreeting(greeting)
    }

    fetchData()
  }, [random])

  useEffect(() => {
    const fetchData = async () => {
      const res = await get(
        '/api/auth-test'
      )

      setIdentityResponse(JSON.stringify(res.data))
    }

    fetchData()
  }, [user])

  // shamlessly stolen from https://stackoverflow.com/a/55647571/4391158 in case you're wondering
  // this should be in the context but it's ok here for demo purposes
  const sendRequest = useCallback(async () => {
    if (isSending) return
    setIsSending(true)
    const { data: { date } } = await get('/api/date')
    if (isMounted.current) {
      setIsSending(false)
      setDateProperty(date)
    }
  }, [isSending])

  return (
    <Layout>
      <h1>{title}</h1>
      <h2>getInitialProps Test</h2>

      <p>
        Original value from SSR: {originalDate}<br />
        Current value from SSR/Update: {dateProperty}
      </p>

      <button disabled={isSending} onClick={sendRequest}>New Date Please</button>

      <h2>Functions Test</h2>
      Result of function: {greeting}
      <br />
      <button onClick={() => setRandom(Math.random())}>New Number</button>

      <h2>Login</h2>

      <a href="/api/auth/login">Login</a><br />
      <a href="/api/auth/logout">Logout</a>

      <p>
        Logged in as: {user ? <strong>{user.username}</strong> : <em>Not logged in.</em>}
      </p>

      <p>
        Response from accessing identity and APIs through zeit functions:
        {' ' + identityResponse}
      </p>
      <h2>Routing</h2>
      <Link href='/temp-page'>
        Temp Page
      </Link>
      <h2>Markdown Copy</h2>
      <HomeContent />
      <ul>
        {cats.map((cat, k) => (
          <li key={k}>
            <h2>{cat.name}</h2>
            <p>{cat.description}</p>
          </li>
        ))}
      </ul>
    </Layout>
  )
}

Home.propTypes = {
  date: PropTypes.string
}

Home.defaultProps = {
  date: ''
}

Home.getInitialProps = async function () {
  return {
    date: getSimpleDate()
  }
}

export default Home
