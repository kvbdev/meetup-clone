import React from 'react'
import Head from 'next/head'
import PropTypes from 'prop-types'

import { i18n, withTranslation } from '../i18n'

import LocalDemo from '../components/presentational/LocalDemo'

/**
 * Demo page showing localisation
 */
const LocalDemoPage = ({ t }) => {
  return (
    <>
      <Head>
        <title>Localisation page demo</title>
      </Head>
      <main>
        <main>
          <h1>{t('h1')}</h1>
          <div>
            <button
              type='button'
              onClick={() => i18n.changeLanguage(i18n.language === 'en' ? 'de' : 'en')}
            >
              {t('change-locale')}
            </button>
            <button
              type='button'
              onClick={() => i18n.changeLanguage('jp')}
            >
              日本語
            </button>
          </div>
          <h2>{t('main-section')}</h2>
          <div><LocalDemo /></div>

        </main>
        <footer>
          <h2>Footer</h2>
          <div className='main-text'>This text is not translated.</div>
        </footer>

      </main>

      <style jsx>{`
        .main-text {
          color: red;
        }
      `}</style>
    </>
  )
}

LocalDemoPage.getInitialProps = async () => ({
  namespacesRequired: ['common'],
})

LocalDemoPage.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('common')(LocalDemoPage)
