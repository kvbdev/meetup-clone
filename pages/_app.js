import React from 'react'
import PropTypes from 'prop-types'
import App from 'next/app'
import { AuthProvider } from '../context/AuthContext'
import { appWithTranslation } from '../i18n'

class MyApp extends App {
  render () {
    const { Component, pageProps } = this.props
    return (
      <>
        <AuthProvider initialState={{ user: null, token: null }}>
          <Component {...pageProps} />
        </AuthProvider>
      </>
    )
  }
}

MyApp.propTypes = {
  Component: PropTypes.func.isRequired,
  pageProps: PropTypes.object.isRequired
}

export default appWithTranslation(MyApp)
