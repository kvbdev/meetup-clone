export function getSimpleDate () {
  return new Date()
    .toISOString()
    .replace(/T/, ' ')
    .replace(/\..+/, '')
}
