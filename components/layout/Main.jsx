import React, { useEffect, useContext } from 'react'
import { get } from 'axios'
import { AuthContext } from '../../context/AuthContext'
import { extractUserProfileObject } from '../../lib/auth/user'

const Main = ({ children }) => {
  const { actions: { setUser } } = useContext(AuthContext)

  useEffect(() => {
    const fetchData = async () => {
      const profile = await get('/api/auth/profile')
      setUser(extractUserProfileObject(profile.data))
    }

    fetchData()
  }, [])

  return (
    <>
      {children}
    </>
  )
}

export default Main
