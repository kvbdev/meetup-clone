# Meetup Clone

A clone of meetup.com. To be adapted for and marketed to local markets.

## Features

Core features required to make it basically work. Actors are highlighted with _italics_ and entities in the system are highlighted in **bold**.

- A _visitor_ should be able to see upcoming **events** immeditely on the front page so that they can start interacting quickly.
- A _visitor_ should be able to localize the language to thier region so they can navigate the page.
- A _visitor_ should be able to browse for **events** they might want to attend so that they may begin to interact with those **events**.
- A _visitor_ should be able to see details of an **event** so they may decide if they wish to attend.
- A _vistior_ should be able to authenticate and become a _user_ so that they may interact with area of the site that need to be linked with an identity.
- A _user_ should be able to create a **group** that acts as a hub for **events** they host.
- A _user_ should be able to create an **event** so that other _users_ may attend, they should then become an _event organiser_.
- An _event organiser_ should be able to assign predefined tags to thier group to help with categorisation and discovery.
- A _user_ should be able to notify an _event organiser_ of thier intent to attend an **event** (becoming an _atendee_) so that the _event organiser_ may plan for it.
- An _event organiser_ should be able to kick out _atendees_ from thier event.
- A _user_ should be able to log out so they are able to keep thier **account** safe on devices they don't own.
- An _administrator_ should be able to edit **event** and **group** details to prevent abuse.
- An _administrator_ should be able to remove problem _user_ accounts from the system to prevent abuse.

Important features.

- A _visitor_ should be able to authenticate via a third party to ease the onboarding process.
- An _atendee_ should be able to leave publicly visible **comments** on event details page so that they may communicate with others.
- An _atendee_ should be able to tag another _atendee_ so that they may be notified that someone has mentioned them.
- An _event organiser_ should be able to leave both private and public comments on event details pages.
- _Atendees_ and _event organisers_ should be able to message each other directly so that questions may be answers and issues be resolved.
- A **user** should be able to join a **group** to see activity related to that group.
- A user should be able to quickly search for **events**, **groups** and **tags** to give more fine control of what they're looking for.

Nice to haves. (i.e. this is the list that never ends, it goes on and on my friends...)

- An _event organiser_ should have the ability to promote thier **event**, giving it a higher weighting in any **event** listing.
- An _event organiser_ should be able to apply to have thier **event** featured on the front page.
- A _user_ should be able to register to receive suggestions of **events** or **groups** they might like.
- A _user_ should be able to contact _administrators_ through the system for any reason without authenticating.
- More to come as I think of them...

# Tech Stack

**Hosting and Backend**

We are looking for a hands-off solution so we are going with [Zeit](https://zeit.co/) and it's integration with [FaunaDB](https://fauna.com/).

We will also be making use of [Auth0](https://auth0.com/) and [Zeit Functions](https://zeit.co/docs/v2/serverless-functions/introduction)

**View Layer**

We need a lightweight solution we can easily host on Zeit and we need server-side rendering so that use content is rendered on the server at runtime.

For this reason we have decided to go with [Next.js](https://nextjs.org/) over Gatsby.js.

This comes with the descision to use [React](https://reactjs.org/) as a view layer.

**State Management**

To reduce the amount of boilerplate needed we will be favouring a combination of [React Context](https://reactjs.org/docs/context.html) and [Hooks](https://reactjs.org/docs/hooks-reference.html#usecontext) to implement a [Flux](https://github.com/facebook/flux)-like architecture of our own. If we need to we can look into redux a bit down the line.

**Styling and CSS**

We use [styled-components](https://www.styled-components.com/) for our styling needs. Flexible enough, behaves like CSS while still living with the components in a JSS-like fasion.

**Testing**

We use [Jest](https://jestjs.io) + [React Testing Library](https://testing-library.com/docs/react-testing-library/intro) as the basic unit testing setup. Both tools are now considered _default_ suggested options and have very good community documentation. In particular, the official Next.JS [example](https://github.com/zeit/next.js/tree/canary/examples/with-jest-react-testing-library) uses Jest + React Testing Library

While Enzyme was considered, the general opinion prescribed is to test implementation, whereas RTL is more focussed around testing user behavior. The consensus is now moving toward the latter as a _better_ approach. This is because you're thinking of your user interaction primarily, rather than thinking of props and state objects which may or may not provide the user experience you want.

As Kent C Dodd puts it:

> The more your tests resemble the way your software is used, the more confidence they can give you.

Test files are *.test.js in the folder of what is being tested except for pages which test in the test folder due to the pages folder being *special\* in the way it is handled by Next.

# Running Locally

**Requirements**

Minimum version for this project is Node >=12.

**Installation**

First, install dependencies.

    yarn install

Then the following commands will be available to you

- `yarn dev` - starts the development build of Next.js. (no now cli)
- `yarn build` - produces a production build of the Next.js application. (no now cli)
- `yarn start` - starts `npx now dev` this will start `now dev` platform and also make Zeit platform specific functionality like providing a proxy for functions. More information about the dev server is provided below.
- `yarn now` - allows you to access `now` without installing it globally.
- `yarn test` - run tests.
- `yarn test-watch` - runs tests in watch mode
- `yarn lint` - runs eslint and gives you a report.
- `yarn format` - runs eslint and tries to fix errors for you

Full documentation about Zeit dev cli [can be found here](https://zeit.co/docs/now-cli/)

# Creating API Routes

API methods simply use the built in [next.js API routing](https://nextjs.org/docs/api-routes/introduction).

A word on architecture though, some pages might need to access data that would usualy be retreived by an API route in `getInitialProps` for this reason we want to keep out API routes "dumb". They shouldn't do anything beyond:

- Receive request an extract required parameters.
- Call some library code in `lib`.
- Return an appropriate response.

# Accessing user identity

On the frontend, the current user is avaiable to the developer via `context/AuthContext` all children of all pages have automatic access to the properties of this context via the custom `pages/_app.js` component. However, the identity events are triggered in `components/layout/Main` so your pages should be wrapped in this main layout component.

As the name suggests this is also where we will be providing the main layout for the application.

To access the context from a `page` simply call the `useContext` hook like so:

    import React, { useContext, useEffect } from 'react'
    import Layout from '../components/layout/Main'
    import { AuthContext } from '../context/AuthContext';

    const Page = () => {
      const { state: { user, token } } = useContext(AuthContext);
      return (
        <Main>
          // sub components for the page
        </Main>
      )
    }

# Linting

Make sure code formatting for this project is running under eslint. Careful about conflicting code formatters, if you wish you can set these settings in the workspace settings or choose not to at all, just be aware that linting errors and testing errors will prevent you from pushing up your work. For a quick autofix you can run `yarn format`.

For VS Code users the relevant settings are:

    "eslint.format.enable": true,
    "editor.codeActionsOnSave": {
      "source.fixAll.eslint": true
    },

This project tries to keep the linting lightweight but if there are problems that linting rule solve then we'll be open to adding them.

The primary rules set we have set are:

- [react/recommended](https://github.com/yannickcr/eslint-plugin-react)
- [standard](https://standardjs.com/)
- [jsx-a11y/recommended](https://github.com/evcohen/eslint-plugin-jsx-a11y)

And they are extended by:

- [react/destructuring-assignment](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/)destructuring-assignment.md
- [react/no-unused-prop-types](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-unused-prop-types.md)
- [react/prefer-stateless-function](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/)
- [react/require-default-props](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/require-default-props.md)
- [react/jsx-key](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-key.md)
- [react/jsx-uses-react](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-uses-react.md)

# JSDoc

JSDoc has been selected as the code commenting, documentation and type hinting engine. This is a lighter touch than Flow or Typescript but still provides many of the benefits. It also has a nice auto-documentation feature.

Using JSDoc allows a much better developer experience for the following reasons

- Commenting to document functions and components is of a consistent documented style
- Type hinting warns you when you try to use an incorrect type in a function call
- Type hinting warns you when you try to use an incorrect prop type when using a component
- IDEs (e.g. VSC) can display inline documentation of your functions when calling
- IDEs (e.g. VSC) can display inline documentation of your prop types when using a component
- IDEs (e.g. VSC) can autocomplete valid props when using a component
- HTML documentation can be extracted easily from your code base

To enable JSDoc base type checking is visual studio code, add the following to your settings.json:

    "javascript.implicitProjectConfig.checkJs": true

the above config setting makes VSCode give you a warning if you are trying to pass a param into a function with the wrong type.

## Functions

The standard syntax for a function:

```javascript
/**
 * Returns one string when supplied with 2 strings
 * **e.g. testStrings('test1', 'test2')**
 * @param {string} string1 First string
 * @param {string} string2 Second string
 * @returns {string} Both string with a comma
 */
const testStrings = (string1, string2) => {
  const returnString = `${string1}, ${string2}`;
  return returnString;
};
```

## React components - basic setup

The standard syntax for JSDoc for a component:

```javascript
/**
 * Test component for checking sytax
 * Renders a message supplied as testItem in the props.
 * @param {object} props React component props
 * @param {string} props.testItem String to render
 */
const TestComponent = (props) => {
  const { testItem } = props;
  return <div>{testItem}</div>;
};

TestComponent.propTypes = {
  testItem: PropTypes.string.isRequired,
};
```

# JSDoc with better-docs

Using the better-docs system, it is possible to autogenerate interactive documentation. This includes examples for React components with editable code that you can try out different props with.

To generate docs:
`yarn docs`

Then go to the generated-docs folder, and open `index.html` in the browser. Open a react component, then click on `Modify Example Code`.

## React components - advanced setup that allows interactive example preview embedded in docs

```javascript
import React from "react";
import PropTypes from "prop-types";

/**
 * Some documented component
 *
 * @component
 * @example
 * const size = 12
 * const text = 'I am documented!'
 * return (
 *   <Documented size={size} text={text} />
 * )
 */
const Documented = (props) => {
  const { text, size } = props;
  return (
    <p style={{ fontSize: size, padding: 10, border: "1px solid #ccc" }}>
      {text}
    </p>
  );
};

Documented.propTypes = {
  /**
   * Text is a text :)
   */
  text: PropTypes.string.isRequired,
  /**
   * Font size
   */
  size: PropTypes.number,
};

Documented.defaultProps = {
  text: "Hello World",
  size: 12,
};

export default Documented;
```

# Internationalisation

Internationalisation is provided by the next-i18next package. There are some issues with deployment that are still being resolved upstream.

Locale specific translations can be found in `./public/static/locales` under specific language folders.

Common translations are located in `common.json` with specific namespaced assets under named files.

To avoid the entire translation dataset being sent to every individual page, specific namespaces should be applied. For example

```javascript
LocalDemoPage.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});
```

The above example limits only the `common` namespaced assets to be sent to the page.

When exporting a component, it should be exported with the withTranslation hook.

```javascript
export default withTranslation("localDemo")(LocalDemo);
```

This injects the `t()` function which allows conversion of defined string elements to a local language. The `localDemo` parameter defines the namespace to help determine which asset in the locales folder is retreived.

For example

```javascript
const LocalDemo = ({ t }) => {
  return (
    <>
      <div>{t("description")}</div>
    </>
  );
};
```

In the above example the `t('description')` block is rendered as whatever the `description` asset is set to in the local language.

## Implementation (Work in progress)

SSR is a very new feature in next-i18next and not only does it mean we're using and unstable version of the package itself but an "experimental" API for next.js.

So it's a bit risky but so long as they don't completely scrap the underlying API I don't see why we won't be able to replace it with something more stable when it comes out. Worst comes to worst we'll have to rewrite some stuff but it should be OK for now.

### References

- The comment that allowed us to implement this - https://github.com/isaachinman/next-i18next/issues/274#issuecomment-656308887
- Example repository - https://github.com/isaachinman/next-i18next-vercel
- RCF discussing next.js rewrites - https://github.com/vercel/next.js/discussions/9081

# Secrets

The term "secrets" is a catch-all term for things related to access control and environment specific configuration. There are two places secrets are kept within production. GitLab [CI/CD environment variables](https://docs.gitlab.com/ee/ci/variables/) for hiding keys and URLs from the build logs and the [now secret](https://zeit.co/docs/v2/serverless-functions/env-and-secrets) command for application specific variables.

Now secret environment variables are not accessible though `now dev` so a developer will also need to copy the .env.example to .env and set the values there.

# Deployments

Gitlab CI handles the deployment. For the time being there are only three stages defined in the .gitlab-ci.yml:

- test
- deploy_preview
- deploy

`test` performs code checks such as linting and running automated tests. Both the following stages need this stage to pass in order to continue.

`deploy_preview` will always deploy a version of the current commnit and put it up at a new URL. Check the delpoy list in the Zeit dashboard to find the URL for it and interact with things like logs.

`deploy` is the same as `deploy_preview` but it will deploy to the URL set as the production deploy and it will only ever run on master.
